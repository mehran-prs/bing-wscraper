#!/usr/bin/env bash

# scrap images, compress,and then remove downloaded images.
# call by year and pages count. e.g: `./get.sh 2012 1` mean that get for year 2012 and this year have 1 page.

python run clear_all
scrapy crawl bing -a year=$1 -a pages=$2
./export.sh $1


