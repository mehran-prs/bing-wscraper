#-----------------------------------
# Docker file to make scrapy image.
#-----------------------------------
#
#   Notes: Don't forget to crate volume
#   for project src.
#
#

FROM python:3

# Install mysql-client to dump database after scrap
# Note: we don't need to this command, but leave it maybe
# in next versions of this crawler need to database.
RUN apt-get update && apt-get install -y mysql-client && rm -rf /var/lib/apt

# Set the working directory to /usr/src/app.
WORKDIR /usr/src/app

# Copy the file from the local host to the filesystem
# of the container at the working directory.
COPY requirements.txt ./

# Install Scrapy specified in requirements.txt.
RUN pip3 install --no-cache-dir -r requirements.txt

CMD ["python3","scrapy","crawl","bamilo","-a","year=2019"]