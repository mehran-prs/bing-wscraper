## Bing wallpapers crawler.

##### This crawler scrap [bingwallpaperhd](bingwallpaperhd.com) website. 

### How to use: 

#### Fast way(Auto clear old files,download,compress):
* in `app` service of composer run 
`./get.sh {year} {pages count}` 
e.g `./get.sh 2016 20` 
(means that get 12 first page of 2016 year).

### slow way (manual download,extract,clear files)
* run composer, in `app` service bash 
(`docker-compose run app bash`) run bing 
crawler and also pass `year`(year that 
should extract) and `pages`(pages count).  
e.g: `scrapy crawl bing -a year=2012 pages=1`

* to extract files as compressed file run :
`./extract.sh {year}` e.g: `./extract.sh 2012`

* to remove downloaded images run `python run clear_all`
