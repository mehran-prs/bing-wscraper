#!/usr/bin/env bash

# How to call: just pass year that your extracted to
# use as compressed file name. e.g : ./extract.sh 2012

export_env(){
    source .env

    # export all .env file variables , skipping comments.
    export $(grep --regexp ^[A-Z] .env | cut -d= -f1)

    return 0
}

compress(){

 tar -zcf $EXPORT_PATH/$YEAR.tar.gz --directory=$FINAL_PATH \
 ./download || return 1

 return 0
}

export FINAL_PATH=/usr/src/app/storage
export YEAR=$1

if export_env && compress; then
    echo "Exported all images to $YEAR.tar.gz file. ";
fi