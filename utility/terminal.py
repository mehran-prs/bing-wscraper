from utility.bcolors import bcolors


def tprint(modifier, val):
    print(modifier + str(val) + bcolors.ENDC)


def success(val):
    """ Print string with success color in terminal """
    tprint(bcolors.OKGREEN, val)


def info(val):
    tprint(bcolors.OKBLUE, val)


def warning(val):
    tprint(bcolors.HEADER, val)


def err(val):
    tprint(bcolors.FAIL, val)
