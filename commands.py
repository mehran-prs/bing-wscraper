import shutil

from utility.terminal import *


def test():
    """
    Just test method :)

    :return: None
    """
    success('Test is ok.')


def clear():
    """
    Clear log file

    :return:None
    """
    open('logs/log.log', 'w').close()
    success("Log file cleared.")


def clear_images():
    """
    Remove crawled images:
    :return:
    """
    shutil.rmtree('/usr/src/app/storage/download')
    success("Crawled images removed.")


def clear_all():
    """
    Remove all downloaded images,clear log file, refresh database.

    :return:None
    """
    clear()
    clear_images()

    success("Removed all assets and logs.")
