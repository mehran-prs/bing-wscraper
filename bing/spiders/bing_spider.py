import scrapy
import scrapy
import logging
import re


class BingSpider(scrapy.Spider):
    name = "bing"

    base = 'http://www.bingwallpaperhd.com'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.validate_arguments()

        self.year = int(self.year)
        self.pages = int(self.pages)

    def start_requests(self):
        for i in range(1, self.pages + 1):
            yield scrapy.Request(url=self.page_url(i), callback=self.parse)

    def parse(self, response):
        """ Just extract all images of page. """
        yield {
            'file_urls': self.extract_images(response)
        }

    def extract_images(self, response):
        images = response.css('.view.view-first>a:first-child>img').xpath('@src').getall()
        new_images = []
        # Remove postfix of images size that has in some images (e.g -1920x1080)
        for src in images:
            new_images.append(re.sub('-\d+x\d+', '', src))

        return new_images

    def validate_arguments(self):
        if self.year is None or self.pages is None:
            raise Exception('To crawl by {} spider, please pass year and pages '
                            'count as command arguments when call to this spider.')

    def page_url(self, page_num):
        return BingSpider.base.rstrip('/') + '/{}/page/{}'.format(self.year, page_num)
